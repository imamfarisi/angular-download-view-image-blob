import { Component } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ApiService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  src : SafeUrl = ''

  constructor(private apiService: ApiService,
    private sanitizer : DomSanitizer) { }

  download() {
    this.apiService.getFile().subscribe(result => {
      const anchor = document.createElement('a');
      anchor.download = "photo.jpg";
      anchor.href = (window.webkitURL || window.URL).createObjectURL(result.body as any);
      
      //bypass security
      this.src = this.sanitizer.bypassSecurityTrustUrl(anchor.href)

      anchor.click();
    })
  }
}
