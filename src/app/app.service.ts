import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(private http: HttpClient) { }

    getFile(): Observable<any> {
        return this.http.get<HttpResponse<Blob>>('https://reqres.in/img/faces/1-image.jpg', { responseType: 'blob' as 'json', observe: 'response' })
    }
}